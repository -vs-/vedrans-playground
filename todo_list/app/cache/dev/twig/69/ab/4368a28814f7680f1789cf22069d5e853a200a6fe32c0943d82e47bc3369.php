<?php

/* VedranTodoBundle:Default:login.html.twig */
class __TwigTemplate_69ab4368a28814f7680f1789cf22069d5e853a200a6fe32c0943d82e47bc3369 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("VedranTodoBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "VedranTodoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Todo List project";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "

            <div class=\"container\">

                <form class=\"form-signin\" method=\"post\" action=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("todo_login");
        echo "\">
                    <h2 class=\"form-signin-heading\">Please sign in</h2>

                    <h2 class=\"form-signin-heading\">Response: ";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["isLoged"]) ? $context["isLoged"] : $this->getContext($context, "isLoged")), "html", null, true);
        echo "</h2>

                    <div class=\"bs-callout bs-callout-warning\">
                        <h4>Login informations:</h4>
                        <p>For testing purposes you can use following credentials:</p>
                        <p>
                            Username: <code>vs</code>, Password: <code>vs</code>,<br />
                            Username: <code>peter</code>, Password: <code>peter</code>,<br />
                            Username: <code>user</code>, Password: <code>user</code>
                        </p>
                    </div>

                    <input name=\"username\" type=\"text\" class=\"form-control\" placeholder=\"Username\" required autofocus>
                    <input name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Password\" required>
                    <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Sign in</button>
                </form>

            </div> <!-- /container -->



        ";
    }

    public function getTemplateName()
    {
        return "VedranTodoBundle:Default:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 13,  44 => 10,  38 => 6,  35 => 5,  29 => 3,);
    }
}
