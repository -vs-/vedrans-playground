<?php

/* VedranTodoBundle::layout.html.twig */
class __TwigTemplate_6758ebebe96d2742b53d063ee2af767b44861a569c7ef38998c9a564ef92e35f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "</head>
<body>

<div id=\"content\">
    ";
        // line 23
        $this->displayBlock('content', $context, $blocks);
        // line 24
        echo "</div>
";
        // line 25
        $this->displayBlock('javascripts', $context, $blocks);
        // line 28
        echo "</body>
</html>";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Test Application";
    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 9
        echo "        <link href=\"/bower_components/bootstrap/dist/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\"/>
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vedrantodo/css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>
        <![endif]-->
    ";
    }

    // line 23
    public function block_content($context, array $blocks = array())
    {
    }

    // line 25
    public function block_javascripts($context, array $blocks = array())
    {
        // line 26
        echo "    <script data-main=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vedrantodo/js/main.js"), "html", null, true);
        echo "\" src=\"/bower_components/requirejs/require.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "VedranTodoBundle::layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  88 => 26,  85 => 25,  80 => 23,  67 => 10,  64 => 9,  61 => 8,  55 => 6,  48 => 25,  45 => 24,  43 => 23,  37 => 19,  30 => 6,  23 => 1,  50 => 28,  44 => 10,  38 => 6,  35 => 8,  29 => 3,);
    }
}
