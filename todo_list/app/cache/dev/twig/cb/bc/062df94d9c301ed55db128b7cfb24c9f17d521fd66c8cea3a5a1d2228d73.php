<?php

/* VedranTodoBundle:Default:index.html.twig */
class __TwigTemplate_cbbc062df94d9c301ed55db128b7cfb24c9f17d521fd66c8cea3a5a1d2228d73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("VedranTodoBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "VedranTodoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Todo List project";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
            <div class=\"navbar navbar-inverse navbar-fixed-top\">
                <div class=\"container\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"#\">Project name</a>
                    </div>
                    <div class=\"collapse navbar-collapse\">
                        <ul class=\"nav navbar-nav\">
                            <li class=\"active\"><a href=\"#\">Home</a></li>
                            <li><a href=\"#about\">About</a></li>
                            <li><a href=\"#contact\">Contact</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>

            <h1>Hello, ";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["username"]) ? $context["username"] : $this->getContext($context, "username")), "html", null, true);
        echo "!</h1>

            <div class=\"container\" ng-app>


                ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lists"]) ? $context["lists"] : $this->getContext($context, "lists")));
        foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
            // line 33
            echo "                    <div ng-controller=\"TodoCtrl\" class=\"liststicker\">

                        <h2>Todo List: ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["list"]) ? $context["list"] : $this->getContext($context, "list")), "name"), "html", null, true);
            echo "</h2>
                        <input type=\"text\" ng-model=\"listName\"
                               placeholder=\"Add List name\" value=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["list"]) ? $context["list"] : $this->getContext($context, "list")), "name"), "html", null, true);
            echo "\">
                        <br />
                        <br />


                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "

                ";
        // line 111
        echo "
                <div ng-controller=\"TodoCtrl\" class=\"liststicker\">
                    <h2>Todo List: {{ listName }}</h2>

                    <input type=\"text\" ng-model=\"listName\"
                           placeholder=\"Add List name\">

                    <br />
                    <br />

                    <span>{{remaining()}} of {{todos.length}} remaining</span>
                    [ <a href=\"\" ng-click=\"archive()\">Clear completed</a> ]
                    <ul class=\"unstyled\">
                        <li ng-repeat=\"todo in todos\">
                            <input type=\"checkbox\" ng-model=\"todo.done\">
                            <span class=\"done-{{todo.done}}\">{{todo.text}}</span>
                        </li>
                    </ul>
                    <form ng-submit=\"addTodo()\">
                        <input type=\"text\" ng-model=\"todoText\"  size=\"30\"
                               placeholder=\"add new todo here\">
                        <input class=\"btn-primary\" type=\"submit\" value=\"add\">
                    </form>

                    <br />
                    <br />

                    <p>
                        <input class=\"btn-primary btn-large\" ng-click=\"postData()\" type=\"button\" value=\"Save List\">
                    </p>

                </div>


                <div ng-controller=\"TodoCtrl\" class=\"liststicker\">
                    <h2>Todo List: {{ listName }}</h2>

                    <input type=\"text\" ng-model=\"listName\"
                           placeholder=\"Add List name\">

                    <br />
                    <br />

                    <span>{{remaining()}} of {{todos.length}} remaining</span>
                    [ <a href=\"\" ng-click=\"archive()\">Clear completed</a> ]
                    <ul class=\"unstyled\">
                        <li ng-repeat=\"todo in todos\">
                            <input type=\"checkbox\" ng-model=\"todo.done\">
                            <span class=\"done-{{todo.done}}\">{[{todo.text}]}</span>
                        </li>
                    </ul>
                    <form ng-submit=\"addTodo()\">
                        <input type=\"text\" ng-model=\"todoText\"  size=\"30\"
                               placeholder=\"add new todo here\">
                        <input class=\"btn-primary\" type=\"submit\" value=\"add\">
                    </form>

                    <br />
                    <br />

                    <p>
                        <input class=\"btn-primary btn-large\" ng-click=\"postData()\" type=\"button\" value=\"Save List\">
                    </p>

                </div>
                ";
        echo "

            </div><!-- /.container -->


        ";
    }

    public function getTemplateName()
    {
        return "VedranTodoBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 111,  95 => 44,  82 => 37,  77 => 35,  73 => 33,  69 => 32,  61 => 27,  38 => 6,  35 => 5,  29 => 3,);
    }
}
