/**
 * Author: Vedran Stankovic <email@vedranstankovic.com>
 * Date: 4.11.13.
 * Time: 11:07
 */

require.config({
    paths: {
        angular: '../../../bower_components/angular/angular',
        angularRoute: '../../../bower_components/angular-route/angular-route',
        angularMocks: '../../../bower_components/angular-mocks/angular-mocks',
        text: '../../../bower_components/requirejs-text/text',
        jquery: '../../../bower_components/jquery/jquery.min',
        bootstrap: '../../../bower_components/bootstrap/dist/js/bootstrap.min',
        todo: 'todo'
    },
    baseUrl: 'bundles/vedrantodo/js',
    shim: {
        'angular' : {'exports' : 'angular'},
        'angularRoute': ['angular'],
        'angularMocks': {
            deps:['angular'],
            'exports':'angular.mock'
        }
    },
    priority: [
        "angular"
    ]
});

// hey Angular, we're bootstrapping manually!
window.name = "NG_DEFER_BOOTSTRAP!";

require( [
    'order!angular',
    'order!app',
    'order!routes',
    'order!jquery',
    'order!bootstrap',
    'order!todo'
], function(angular, app, routes, jquery, bootstrap) {
    'use strict';
    var $html = angular.element(document.getElementsByTagName('html')[0]);

    angular.element().ready(function() {
        $html.addClass('ng-app');
        angular.bootstrap($html, [app['name']]);
    });


});

