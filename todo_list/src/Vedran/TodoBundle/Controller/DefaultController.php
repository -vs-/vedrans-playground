<?php

namespace Vedran\TodoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use r;

class DefaultController extends Controller
{

    /*
     */
    public function indexAction()
    {
        $username = $this->container->get('security.context')->getToken()->getUser();

        $conn = r\connect('localhost');
        $lists = r\db("todoapp")
            ->table("list")
            ->run($conn)->toNative();


        return $this->render('VedranTodoBundle:Default:index.html.twig', array(
            'username'  => $username,
            'lists'     => $lists
        ));
    }

    /*
     */
    public function loginAction(Request $request)
    {

        if($request->getMethod() == 'POST' && $request->get('username') != null && $request->get('password') != null){
            $username = $request->get('username');
            $password = $request->get('password');

            // Connect to localhost
            $conn = r\connect('localhost');
            $result = r\db("todoapp")
                ->table("user")
                ->filter(
                    array('username' => $username, 'password' => $password))
                ->count()
                ->run($conn)->toNative();


            if($result > 0){

                $user = $username;
                $pass = $password;
                $roles = array('ROLE_USER');

                $token = new UsernamePasswordToken($user, $pass, 'secured_area', $roles);

                $request = $this->getRequest();
                $session = $request->getSession();
                $session->set('_security_secured_area',  serialize($token));

                $router = $this->get('router');
                $url = $router->generate('todo_index');

                return $this->redirect($url);


            }else{
                $isLoged = 'No!';
            }

        }else{
            $isLoged = 'Not even tried...';
        }
        return $this->render('VedranTodoBundle:Default:login.html.twig', array('isLoged' => $isLoged));
    }

    public function logoutAction(){
        //nothing to see here
    }

    public function postAction(Request $request){


        $content = $request->getContent();
        if (!empty($content))
        {
            $content = json_decode($content, true); // 2nd param of json_decode to get as array

            $conn = r\connect('localhost');
            $post = r\db("todoapp")
                ->table("list")
                ->insert($content)
                ->run($conn);

            $post = 'Done!';


        }else{
            $post = 'nothing retreived from post...';
        }

        return $this->render('VedranTodoBundle:Default:post.html.twig', array('post' => $post));

    }
}
