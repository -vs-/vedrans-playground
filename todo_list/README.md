Todo List test project
========================

Welcome to the Todo List test project. At the end, project would consist of following:

1) Symfony2 Framework
----------------------------------
PHP framework.

2) RethinkDB
----------------------------------
RethinkDB as a persistent storage, this is similar to MongoDB but it uses JSON instead of BSON.

3) AngularJS
----------------------------------
Google js framework for ajax and "one page" approach. AngularJS will be downloaded and included via angular-require-seed
with help of RequrireJS.

4) Bootstrap 3
----------------------------------
Newest version of planetary popular client side framework that includes many useful functions such as responsive grid.
For sake of this project I will not use LESS files but CSS and will leave default design.

5) Composer
----------------------------------
All back end PHP libraries will be available for download via composer

6) Bower
----------------------------------
All front end libraries will be available for download via bower
