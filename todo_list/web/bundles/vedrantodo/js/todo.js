/**
 * Author: Vedran Stankovic <email@vedranstankovic.com>
 * Date: 4.11.13.
 * Time: 14:31
 */

function TodoCtrl($scope, $http) {

    $scope.todos = [
            
        ];


    $scope.addTodo = function() {
        $scope.todos.push({text:$scope.todoText, done:false});
        $scope.todoText = '';
    };

    $scope.remaining = function() {
        var count = 0;
        angular.forEach($scope.todos, function(todo) {
            count += todo.done ? 0 : 1;
        });
        return count;
    };

    $scope.archive = function() {
        var oldTodos = $scope.todos;
        $scope.todos = [];
        angular.forEach(oldTodos, function(todo) {
            if (!todo.done) $scope.todos.push(todo);
        });
    };

    $scope.postData = function(){
        $http({
            method: 'POST',
            url: '/post',
            data: angular.toJson({name:$scope.listName, list: $scope.todos})
        }).success(function () {

            });
    };

}