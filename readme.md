In this folder you can see my showcase. Below are short descriptions of each example.
=======================================================================================

-**android -** very simple android application source files, it is an application for switching silent mode on mobile phone on and off

-**CI -** This is one very simple test application written in CodeIgniter Php framework. It allows registration / login via Facebook and after that it allows searching using Google Places Api.

-**goblog -** This is simple blog platform that uses PostgreSQL as data storage and is written in Golang. Created as hobby while I was playing with Golang, it is far from stable for use in production.

-**html5 -** you can see here an html file (with included assets) which is coded as short showcase of new html5 introduced options and functionalities. There is also monster.html file where you can see "drawing" coded only by using html/css3 combination - no images or graphic files are used for that drawing.

-**js -** these are javascript files that i coded for my website vedranstankovic.com

-**less -** this folder contains less files for patching default bootstrap styling. In that way i keep best things from bootstrap and combine them with my own unique styling which results in beautifully written minimised responsive aware css3 files.

-**mobile_js -** this is example of code I used for checking network status in mobile phone using Apache Cordova set of device APIs for deploying on multiple mobile platforms.

-**php -** part of my own CMS, sample files of php code written in OOP MVC development style. You can see examples of Model class, model extension class, controller class and some view files.

-**rackUploader -**  my own uploading widget for uploading files to Rackspace Cloud Files Akamai CDN. It is wired to their older version of API and it is written for quick and easy including to any php application.

-**todo_list -** example files of todo list project created in Symfony2 with RethinkDB as data storage and AngularJS at client side. NOTE: not all files are included, only example files.
